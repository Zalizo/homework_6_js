/*
1. Екранування - коли спеціальні символи("", ;, /) використовуються не як код,
 а як той елемент, який повинен бути виведений на екран, або записаний у файлі
2. Оголосити функцію можна за допомогою ключ. слова function,
 оголосити функцію як вираз та стрілочна функція.
3. Нoisting - це поведінка js, коли змінні та функції можуть бути використанні до її оголошення.
При обробці коду спочатку відбувається підняття всіх оголошень, і тільки потім виконується код.
*/

function createNewUser() {
  let firstName = prompt("Введіть ваше ім'я:");
  let lastName = prompt("Введіть ваше прізвище:");
  let birthDate = prompt(
    "Введіть дату вашого народження у форматі dd.mm.yyyy:"
  );

  let dateParts = birthDate.split(".");
  let birthYear = parseInt(dateParts[2]);
  let birthMonth = parseInt(dateParts[1]) - 1;
  let birthDay = parseInt(dateParts[0]);
  let birthday = new Date(birthYear, birthMonth, birthDay);

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,
    getLogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
    setFirstName: function (newFirstName) {
      this.firstName = newFirstName;
    },
    setLastName: function (newLastName) {
      this.lastName = newLastName;
    },
    getAge: function () {
      let now = new Date();
      let ageInMilliseconds = now - this.birthday;
      let ageInYears = ageInMilliseconds / (1000 * 60 * 60 * 24 * 365.25);
      return Math.floor(ageInYears);
    },
    getPassword: function () {
      let password = "";
      password += this.firstName[0].toUpperCase();
      password += this.lastName.toLowerCase();
      password += this.birthday.getFullYear();
      return password;
    },
  };

  return newUser;
}

const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

user.setFirstName("Нове ім'я");
user.setLastName("Нове прізвище");
console.log(user.getLogin());
